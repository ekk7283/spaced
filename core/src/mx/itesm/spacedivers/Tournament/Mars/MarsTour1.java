package mx.itesm.spacedivers.Tournament.Mars;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.Iterator;

import mx.itesm.spacedivers.Collectables.BananaOne;
import mx.itesm.spacedivers.Collectables.BlueDiamond;
import mx.itesm.spacedivers.Collectables.GreenDiamond;
import mx.itesm.spacedivers.Collectables.PinkDiamond;
import mx.itesm.spacedivers.Collectables.RedDiamond;
import mx.itesm.spacedivers.Collectables.YellowDiamond;
import mx.itesm.spacedivers.Fuente;
import mx.itesm.spacedivers.Jim;
import mx.itesm.spacedivers.Menu.StartScreen;
import mx.itesm.spacedivers.Obstacles.RockOne;
import mx.itesm.spacedivers.Obstacles.RockTwo;
import mx.itesm.spacedivers.Obstacles.RocketOne;
import mx.itesm.spacedivers.Obstacles.RocketThree;
import mx.itesm.spacedivers.Obstacles.RocketTwo;
import mx.itesm.spacedivers.Obstacles.SateliteOne;
import mx.itesm.spacedivers.Obstacles.SateliteTwo;
import mx.itesm.spacedivers.SpaceDivers;
import mx.itesm.spacedivers.Tournament.Mercury.LoadingScreenMercuryTour;

import static mx.itesm.spacedivers.Tournament.Mars.MarsTour1.STATE.GAME_OVER;
import static mx.itesm.spacedivers.Tournament.Mars.MarsTour1.STATE.GAME_WON;
import static mx.itesm.spacedivers.Tournament.Mars.MarsTour1.STATE.PAUSE;
import static mx.itesm.spacedivers.Tournament.Mars.MarsTour1.STATE.PLAYING;

/**
 * Created by spider-i on 3/26/17.
 */

public class MarsTour1 extends ScreenAdapter {

    private Stage stage;

    private final SpaceDivers game;
    private static final float CELL_SIZE = 32;


    private Jim jimmy;

    private static final float VIRTUAL_WIDTH = 720;
    private static final float VIRTUAL_HEIGHT = 1280;

    private static final float TAMCHAN = 262;

    private static final float BARRA_SIZE = 165;
    private static final float TOP =22480;
    private static final float BARRAPOS = TOP - BARRA_SIZE;
    private static final float BACKGROUND_VELOCITY = TOP-640;

    private static final float CAMERA_SPEED = 170.0f;

    private Fuente font;

    private int score=0;
    private int sumaBlue=150;
    private int sumaYellow=175;
    private int sumaRed=125;
    private int sumaPink=100;
    private int sumaGreen=200;
    private int restObst=25;


    private int vidas=5;
    private int restvida=1;


////////////CAMARA////////

    private float posx=360;
    private ShapeRenderer shapeRenderer;
    private OrthographicCamera camera;
    private Viewport viewport;
    private SpriteBatch batch;

    private float suma=0.0f;
    private int velocity_y = 40;
///////MAPA/////////
    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;
    private Texture barra;
    private Pixmap pixmap;
    private TiledMapTileLayer layer;
    private TiledMapTileMapObject objects;
    private TextureAtlas atlasJim;
    private Sprite jimstatic;
    private Sprite jimmoving;

    /////////ACTORES///////
    private GlyphLayout layout;
    private BitmapFont bitmapFont;
    private BitmapFont bitmapFont2;

    private Label scoreLabel;
    private Label scoreNumber;
    private Label life;
    private Label gameover;
    private Label gamewin;
    private Label scoreFinal;

    ///////WINNER//////////////
    private Texture pantallaVictory;
    private Texture victoryImage;
    private Image imagenVictory;


///////GAMEOVER//////////////
    private Texture pantallaGameOver;
    private Texture gameOverImage;
    private Image imagenGameOver;

//////////PAUSA///////////

    private Texture pantallaPausa;
    private Texture pausaImage;
    private Image imagenPausa;

    ////////////BOTONES//////////
    private ImageButton pause;
    private Texture pausabut;
    private ImageButton resume;
    private Texture resumeTexture;
    private Texture resumeTexturePress;
    private ImageButton restart;
    private Texture restartTexture;
    private Texture restartTexturePress;
    private ImageButton quit;
    private Texture quitTexture;
    private Texture quitTexturePress;
    private ImageButton continueB;
    private Texture continueTexture;
    private Texture continueTexturePress;

    private int idNivel;


    private Vector2 direction;

    ////////OBSTACLES///////////////
    private Array<RockOne>  rockUno = new Array<RockOne>();
    private Array<RockTwo> rockDos = new Array<RockTwo>();
    private Array<RocketOne> rocketUno = new Array<RocketOne>();
    private Array<RocketTwo> rocketDos = new Array<RocketTwo>();
    private Array<RocketThree> rocketTres = new Array<RocketThree>();
    private Array<SateliteOne> sateliteUno = new Array<SateliteOne>();
    private Array<SateliteTwo> sateliteDos = new Array<SateliteTwo>();




    /////////COLLECTIBLES////////////
    private Array<BlueDiamond>  itemseses = new Array<BlueDiamond>();
    private Array<YellowDiamond>  itemsyel = new Array<YellowDiamond>();
    private Array<RedDiamond>  itemsred = new Array<RedDiamond>();
    private Array<GreenDiamond>  itemsgreen = new Array<GreenDiamond>();
    private Array<PinkDiamond>  itemspink = new Array<PinkDiamond>();
    private Array<BananaOne>  itemsBanana = new Array<BananaOne>();



    ////////ESTADO//////////
    public enum STATE {
        PLAYING,PAUSE, GAME_WON,GAME_OVER
    }
    private STATE state = STATE.PLAYING;


    public MarsTour1(SpaceDivers game, int idNivel) {
        this.game = game;
        this.idNivel = idNivel;
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        viewport.update(width, height);
        stage.getViewport().update(width,height,true);
    }

    public void show() {
        super.show();

        ///////// CAMARA, VIEWPORT & STAGE /////////

        stage = new Stage(new FitViewport(VIRTUAL_WIDTH,VIRTUAL_HEIGHT));
        Gdx.input.setInputProcessor(stage);

        camera = new OrthographicCamera();
        camera.position.set(0, TOP, 0);
        viewport = new FitViewport(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, camera);
        viewport.apply(true);


        ////////ACELEROMETRO/////////

        Gdx.input.getAccelerometerX();
        Gdx.input.getAccelerometerY();
        Gdx.input.getAccelerometerZ();


        /////////BOTONES///////////

        pausabut = new Texture(Gdx.files.internal("BotonPausa.png"));
        pause = new ImageButton(new TextureRegionDrawable(new TextureRegion(pausabut)), new TextureRegionDrawable(new TextureRegion(pausabut)));
        pause.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                state = PAUSE;                 game.getAssetManager().get("SpaceLevelSong.mp3", Music.class).pause();                 if (game.isMusicOn()){                     game.getAssetManager().get("UNLOCKFX.wav", Sound.class).play();                 }


            }
        });
        pause.setPosition(VIRTUAL_WIDTH-30,1220,Align.right);



        restartTexture = new Texture(Gdx.files.internal("BotonRestartLibre.png"));
        restartTexturePress = new Texture(Gdx.files.internal("BotonRestartPressed.png"));
        restart = new ImageButton(new TextureRegionDrawable(new TextureRegion(restartTexture)), new TextureRegionDrawable(new TextureRegion(restartTexturePress)));
        restart.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
             dispose();
                game.setScreen(new mx.itesm.spacedivers.Tournament.Mars.LoadingScreenMarsTour(game));


            }
        });
        restart.setPosition((VIRTUAL_WIDTH/2),700,Align.center);


        resumeTexture = new Texture(Gdx.files.internal("Forward.png"));
        resumeTexturePress = new Texture(Gdx.files.internal("ForwardPress.png"));
        resume = new ImageButton(new TextureRegionDrawable(new TextureRegion(resumeTexture)), new TextureRegionDrawable(new TextureRegion(resumeTexturePress)));
        resume.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);

                state = PLAYING;
                imagenPausa.remove();
                gameover.remove();
                quit.remove();
                resume.remove();
                restart.remove();
                if(game.isMusicOn()){ game.getAssetManager().get("SpaceLevelSong.mp3", Music.class).play(); }
            }
        });
        resume.setPosition((VIRTUAL_WIDTH/2),150,Align.center);


        quitTexture = new Texture(Gdx.files.internal("BotonQuitLibre.png"));
        quitTexturePress = new Texture(Gdx.files.internal("BotonQuitPressed.png"));
        quit = new ImageButton(new TextureRegionDrawable(new TextureRegion(quitTexture)), new TextureRegionDrawable(new TextureRegion(quitTexturePress)));
        quit.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);

                game.setScreen(new StartScreen(game));
                dispose();
            }
        });
        quit.setPosition((VIRTUAL_WIDTH/2),400,Align.center);

        continueTexture = new Texture(Gdx.files.internal("BotonContinueLibre.png"));
        continueTexturePress = new Texture(Gdx.files.internal("BotonContinuePressed.png"));
        continueB = new ImageButton(new TextureRegionDrawable(new TextureRegion(continueTexture)), new TextureRegionDrawable(new TextureRegion(continueTexturePress)));
        continueB.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);

                Preferences preferencias = Gdx.app.getPreferences("Highscores");
                int scorePasado = preferencias.getInteger("martet");
                int scoreNuevo= scorePasado+score;
                preferencias.putInteger("martet",scoreNuevo);
                preferencias.flush();

                System.out.println(scoreNuevo);

                game.setScreen(new LoadingScreenMercuryTour(game));
                dispose();
            }
        });


        layout = new GlyphLayout();
        bitmapFont = new BitmapFont(Gdx.files.internal("Titu.fnt"));
        bitmapFont2 = new BitmapFont(Gdx.files.internal("DarkF.fnt"));


        shapeRenderer =new ShapeRenderer();
        batch = new SpriteBatch();
        map =  game.getAssetManager().get("MarteMap.tmx");
        renderer = new OrthogonalTiledMapRenderer(map, batch);
        renderer.setView(camera);

        jimmy = new Jim(
                game.getAssetManager().get("jim1.png", Texture.class)

        );
        barra = game.getAssetManager().get("BarraSuperior.png", Texture.class);


        direction = new Vector2();
        Gdx.app.log("LOG","Finished loading");

        font = new Fuente();
        game.getAssetManager().get("SpaceLevelSong.mp3", Music.class);
        if(game.isMusicOn()){ game.getAssetManager().get("SpaceLevelSong.mp3", Music.class).play(); }

        pantallaGameOver = game.getAssetManager().get("GameOver.png",Texture.class);
        imagenGameOver = new Image(new TextureRegionDrawable(new TextureRegion(pantallaGameOver)));
        //////////
        pantallaVictory = game.getAssetManager().get("Victory.png",Texture.class);
        imagenVictory = new Image(new TextureRegionDrawable(new TextureRegion(pantallaVictory)));
    ///////////
        pantallaPausa = game.getAssetManager().get("Paused.png",Texture.class);
        imagenPausa = new Image(new TextureRegionDrawable(new TextureRegion(pantallaPausa)));


        System.out.println(Integer.toString(idNivel));


        seleccionNivel();

        Label.LabelStyle scorlab = new Label.LabelStyle(bitmapFont,Color.WHITE);
        scoreLabel = new Label("SCORE",scorlab);

        Label.LabelStyle lif = new Label.LabelStyle(bitmapFont,Color.WHITE);
        life = new Label(Integer.toString(vidas),lif);

        Label.LabelStyle scnum = new Label.LabelStyle(bitmapFont,Color.WHITE);
        scoreNumber = new Label(Integer.toString(score),scnum);


        Label.LabelStyle gameov = new Label.LabelStyle(bitmapFont,Color.WHITE);
        gameover = new Label("GAME OVER",gameov);

        Label.LabelStyle gamewi = new Label.LabelStyle(bitmapFont2,Color.WHITE);
        gamewin = new Label("SCORE",gamewi);



        stage.addActor(pause);

        stage.addActor(scoreLabel);
        stage.addActor(life);
        stage.addActor(scoreNumber);

        scoreLabel.setPosition((VIRTUAL_WIDTH/2)-5,1250,Align.center);
        life.setPosition(25,1220,Align.left);
        scoreNumber.setPosition((VIRTUAL_WIDTH/2)-35,1155,Align.center);



    }

    private void seleccionNivel(){
        if(idNivel ==0) {
            Obstacles1();
            PlusItems1();
        }
        if (idNivel==1){

            Obstacles2();
            PlusItems2();
        }
        else{

            Obstacles3();
            PlusItems3();
        }
    }

    private void Obstacles1 ( ) {
        MapLayer layer1 = map.getLayers().get("Roca1");//arr[2]
        System.out.println("Si");
        for (MapObject mapObject : layer1.getObjects()) {
            rockDos.add(
                    new RockTwo(
                            game.getAssetManager().get("roca2anim.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }
        System.out.println("Segundo");


        MapLayer layer2 = map.getLayers().get("Ovni1");
        System.out.println("Si");
        for (MapObject mapObject : layer2.getObjects()) {
            rocketUno.add(
                    new RocketOne(
                            game.getAssetManager().get("Nave.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }

        MapLayer layer3 = map.getLayers().get("Nave1");
        System.out.println("Si");
        for (MapObject mapObject : layer3.getObjects()) {
            rocketTres.add(
                    new RocketThree(
                            game.getAssetManager().get("Nave2.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }

        MapLayer layer4 = map.getLayers().get("Satelite1");
        System.out.println("Si");
        for (MapObject mapObject : layer4.getObjects()) {
            sateliteUno.add(
                    new SateliteOne(
                            game.getAssetManager().get("satelite4anim.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }




    }

    private void PlusItems1 ( ) {

        MapLayer layer = map.getLayers().get("Azul1");
        for (MapObject mapObject : layer.getObjects()) {
            itemseses.add(
                    new BlueDiamond(
                            game.getAssetManager().get("DiamAzul.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }
        MapLayer layer1 = map.getLayers().get("Amarillo1");
        for (MapObject mapObject : layer1.getObjects()) {
            itemsyel.add(
                    new YellowDiamond(
                            game.getAssetManager().get("DiamAmarillo.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }

        MapLayer layer2 = map.getLayers().get("Rojo1");
        for (MapObject mapObject : layer2.getObjects()) {
            itemsred.add(
                    new RedDiamond(
                            game.getAssetManager().get("DiamRojo.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }

        MapLayer layer3 = map.getLayers().get("Rosa1");
        for (MapObject mapObject : layer3.getObjects()) {
            itemspink.add(
                    new PinkDiamond(
                            game.getAssetManager().get("DiamRosa.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }
        MapLayer layer4 = map.getLayers().get("Verde1");
        for (MapObject mapObject : layer4.getObjects()) {
            itemsgreen.add(
                    new GreenDiamond(
                            game.getAssetManager().get("DiamVerde.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }

        MapLayer layer5 = map.getLayers().get("Banana1");
        for (MapObject mapObject : layer5.getObjects()) {
            itemsBanana.add(
                    new BananaOne(
                            game.getAssetManager().get("bananas.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }


    }
    //////////////////

    private void Obstacles2 ( ) {

        MapLayer layer1 = map.getLayers().get("Roca2");//arr[2]
        for (MapObject mapObject : layer1.getObjects()) {
            rockDos.add(
                    new RockTwo(
                            game.getAssetManager().get("roca2anim.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }



        MapLayer layer2 = map.getLayers().get("Ovni2");
        for (MapObject mapObject : layer2.getObjects()) {
            rocketUno.add(
                    new RocketOne(
                            game.getAssetManager().get("Nave.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }

        MapLayer layer3 = map.getLayers().get("Nave2");
        for (MapObject mapObject : layer3.getObjects()) {
            rocketTres.add(
                    new RocketThree(
                            game.getAssetManager().get("Nave2.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }

        MapLayer layer4 = map.getLayers().get("Satelite2");
        for (MapObject mapObject : layer4.getObjects()) {
            sateliteUno.add(
                    new SateliteOne(
                            game.getAssetManager().get("satelite4anim.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }


    }

    private void PlusItems2 ( ) {

        MapLayer layer = map.getLayers().get("Azul2");
        for (MapObject mapObject : layer.getObjects()) {
            itemseses.add(
                    new BlueDiamond(
                            game.getAssetManager().get("DiamAzul.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }
        MapLayer layer1 = map.getLayers().get("Amarillo2");
        for (MapObject mapObject : layer1.getObjects()) {
            itemsyel.add(
                    new YellowDiamond(
                            game.getAssetManager().get("DiamAmarillo.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }

        MapLayer layer2 = map.getLayers().get("Rojo2");
        for (MapObject mapObject : layer2.getObjects()) {
            itemsred.add(
                    new RedDiamond(
                            game.getAssetManager().get("DiamRojo.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }

        MapLayer layer3 = map.getLayers().get("Rosa2");
        for (MapObject mapObject : layer3.getObjects()) {
            itemspink.add(
                    new PinkDiamond(
                            game.getAssetManager().get("DiamRosa.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }
        MapLayer layer4 = map.getLayers().get("Verde2");
        for (MapObject mapObject : layer4.getObjects()) {
            itemsgreen.add(
                    new GreenDiamond(
                            game.getAssetManager().get("DiamVerde.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }

        MapLayer layer5 = map.getLayers().get("Banana2");
        for (MapObject mapObject : layer5.getObjects()) {
            itemsBanana.add(
                    new BananaOne(
                            game.getAssetManager().get("bananas.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }


    }
//////////////////////
private void Obstacles3 ( ) {
    MapLayer layer1 = map.getLayers().get("Roca3");//arr[2]
    for (MapObject mapObject : layer1.getObjects()) {
        rockDos.add(
                new RockTwo(
                        game.getAssetManager().get("roca2anim.png", Texture.class),
                        mapObject.getProperties().get("x", Float.class),
                        mapObject.getProperties().get("y", Float.class)
                )
        );
    }



    MapLayer layer2 = map.getLayers().get("Ovni3");
    for (MapObject mapObject : layer2.getObjects()) {
        rocketUno.add(
                new RocketOne(
                        game.getAssetManager().get("Nave.png", Texture.class),
                        mapObject.getProperties().get("x", Float.class),
                        mapObject.getProperties().get("y", Float.class)
                )
        );
    }

    MapLayer layer3 = map.getLayers().get("Nave3");
    for (MapObject mapObject : layer3.getObjects()) {
        rocketTres.add(
                new RocketThree(
                        game.getAssetManager().get("Nave2.png", Texture.class),
                        mapObject.getProperties().get("x", Float.class),
                        mapObject.getProperties().get("y", Float.class)
                )
        );
    }

    MapLayer layer4 = map.getLayers().get("Satelite3");
    for (MapObject mapObject : layer4.getObjects()) {
        sateliteUno.add(
                new SateliteOne(
                        game.getAssetManager().get("satelite4anim.png", Texture.class),
                        mapObject.getProperties().get("x", Float.class),
                        mapObject.getProperties().get("y", Float.class)
                )
        );
    }


}

    private void PlusItems3 ( ) {

        MapLayer layer = map.getLayers().get("Azul3");
        for (MapObject mapObject : layer.getObjects()) {
            itemseses.add(
                    new BlueDiamond(
                            game.getAssetManager().get("DiamAzul.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }
        MapLayer layer1 = map.getLayers().get("Amarillo3");
        for (MapObject mapObject : layer1.getObjects()) {
            itemsyel.add(
                    new YellowDiamond(
                            game.getAssetManager().get("DiamAmarillo.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }

        MapLayer layer2 = map.getLayers().get("Rojo3");
        for (MapObject mapObject : layer2.getObjects()) {
            itemsred.add(
                    new RedDiamond(
                            game.getAssetManager().get("DiamRojo.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }

        MapLayer layer3 = map.getLayers().get("Rosa3");
        for (MapObject mapObject : layer3.getObjects()) {
            itemspink.add(
                    new PinkDiamond(
                            game.getAssetManager().get("DiamRosa.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }
        MapLayer layer4 = map.getLayers().get("Verde3");
        for (MapObject mapObject : layer4.getObjects()) {
            itemsgreen.add(
                    new GreenDiamond(
                            game.getAssetManager().get("DiamVerde.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }

        MapLayer layer5 = map.getLayers().get("Banana3");
        for (MapObject mapObject : layer5.getObjects()) {
            itemsBanana.add(
                    new BananaOne(
                            game.getAssetManager().get("bananas.png", Texture.class),
                            mapObject.getProperties().get("x", Float.class),
                            mapObject.getProperties().get("y", Float.class)
                    )
            );
        }


    }



    @Override
    public void dispose() {
        super.dispose();
        map.dispose();
        renderer.dispose();
        stage.dispose();

    }
    private void updateCameraY(int x) {
        camera.position.set(camera.position.x,BACKGROUND_VELOCITY -x, camera.position.z);
            camera.update();
            renderer.setView(camera);
    }

@Override
    public void render(float delta) {
    super.render(delta);
    switch (state) {
        case PLAYING:
            update(delta);
            break;
        case GAME_OVER:
            addGameOver();
            break;
        case GAME_WON:
            addWin();
            break;
        case PAUSE:
            break;

    }

        clearScreen();
        stage.act(delta);



        draw(delta);
        stage.draw();

    }



    private void clearScreen() {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }


    private void addWin(){


        Label.LabelStyle finalScore = new Label.LabelStyle(bitmapFont2,Color.WHITE);
        scoreFinal = new Label(Integer.toString(score),finalScore);
        restart.setPosition((VIRTUAL_WIDTH/2),350,Align.center);
        continueB.setPosition((VIRTUAL_WIDTH/2),375,Align.center);
        quit.setPosition((VIRTUAL_WIDTH/2),250,Align.center);
        stage.addActor(scoreFinal);
        scoreFinal.setPosition((VIRTUAL_WIDTH/2),475,Align.center);
        stage.addActor(gamewin);
        gamewin.setPosition((VIRTUAL_WIDTH/2),530,Align.center);



    }




    private void addGameOver(){



        restart.setPosition((VIRTUAL_WIDTH/2),375,Align.center);
        quit.setPosition((VIRTUAL_WIDTH/2),275,Align.center);


        }



    private void draw(float delta) {
        batch.setProjectionMatrix(camera.projection);
        batch.setTransformMatrix(camera.view);
        renderer.render();
        batch.begin();
        drawScore();

        /*for (RockOne rock1 : rockUno) {
            rock1.draw(batch);
            rock1.update(delta);
        }
        */
        for (RockTwo rock2 : rockDos) {
            rock2.draw(batch);
            rock2.update(delta);
        }
        for (RocketOne rocket1 : rocketUno) {
            rocket1.draw(batch);
            rocket1.update(delta);
        }
        for (RocketThree rocket3 : rocketTres) {
            rocket3.draw(batch);
            rocket3.update(delta);
        }
        for (SateliteOne satelite1 : sateliteUno) {
            satelite1.draw(batch);
            satelite1.update(delta);
        }
        /*for (SateliteTwo satelite2 : sateliteDos) {
            satelite2.draw(batch);
            satelite2.update(delta);
        }
        */
///////////////////////////////
        for (BlueDiamond recol : itemseses) {
            recol.draw(batch);
            recol.update(delta);
        }
        for (GreenDiamond recol : itemsgreen) {
            recol.draw(batch);
            recol.update(delta);
        }
        for (PinkDiamond recol : itemspink) {
            recol.draw(batch);
            recol.update(delta);
        }
        for (RedDiamond recol : itemsred) {
            recol.draw(batch);
            recol.update(delta);
        }
        for (YellowDiamond recol : itemsyel) {
            recol.draw(batch);
            recol.update(delta);
        }
        for (BananaOne recol : itemsBanana) {
            recol.draw(batch);
            recol.update(delta);
        }


        jimmy.draw(batch);
        batch.draw(barra, 0, BARRAPOS-velocity_y);


        if (state== GAME_OVER){
            stage.addActor(imagenGameOver);
            stage.addActor(quit);
            stage.addActor(restart);

            imagenGameOver.setPosition(0,100);
        }

        if (state== PAUSE){
            stage.addActor(imagenPausa);
            stage.addActor(resume);
            stage.addActor(quit);
            stage.addActor(restart);

            imagenPausa.setPosition(0,100);
            restart.setPosition((VIRTUAL_WIDTH/2),375,Align.center);
            quit.setPosition((VIRTUAL_WIDTH/2),275,Align.center);
        }
        if (state== GAME_WON){
            stage.addActor(imagenVictory);
            stage.addActor(quit);
            stage.addActor(continueB);



            imagenVictory.setPosition(0,100);
        }

        batch.end();




    }

    private void drawDebug() {
        shapeRenderer.setProjectionMatrix(camera.projection);
        shapeRenderer.setTransformMatrix(camera.view);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        jimmy.drawDebug(shapeRenderer);
        shapeRenderer.end();
    }

private void JimCollisions() {
   /* for (RockOne rock1 : rockUno) {
        if (checkCollisions(jimmy.getCollisionRectangle().getX(), jimmy.getCollisionRectangle().getY(), jimmy.getCollisionRectangle().getWidth(),
                jimmy.getCollisionRectangle().getHeight(), rock1.getCollisionRectangle().getX(),
                rock1.getCollisionRectangle().getY(), rock1.getCollisionRectangle().getWidth(),
                rock1.getCollisionRectangle().getHeight())) {

        }
    }
    */
    for (RockTwo rock2 : rockDos) {
        if (checkCollisions(jimmy.getCollisionRectangle().getX(), jimmy.getCollisionRectangle().getY(), jimmy.getCollisionRectangle().getWidth(),
                jimmy.getCollisionRectangle().getHeight(), rock2.getCollisionRectangle().getX(),
                rock2.getCollisionRectangle().getY(), rock2.getCollisionRectangle().getWidth(),
                rock2.getCollisionRectangle().getHeight())) {

        }
    }
    for (RocketOne rocket1 : rocketUno) {
        if (checkCollisions(jimmy.getCollisionRectangle().getX(), jimmy.getCollisionRectangle().getY(), jimmy.getCollisionRectangle().getWidth(),
                jimmy.getCollisionRectangle().getHeight(), rocket1.getCollisionRectangle().getX(),
                rocket1.getCollisionRectangle().getY(), rocket1.getCollisionRectangle().getWidth(),
                rocket1.getCollisionRectangle().getHeight())) {

        }
    }
    for (RocketThree rocket3 : rocketTres) {
        if (checkCollisions(jimmy.getCollisionRectangle().getX(), jimmy.getCollisionRectangle().getY(), jimmy.getCollisionRectangle().getWidth(),
                jimmy.getCollisionRectangle().getHeight(), rocket3.getCollisionRectangle().getX(),
                rocket3.getCollisionRectangle().getY(), rocket3.getCollisionRectangle().getWidth(),
                rocket3.getCollisionRectangle().getHeight())) {

        }
    }

    for (SateliteOne satelite1 : sateliteUno) {
        if (checkCollisions(jimmy.getCollisionRectangle().getX(), jimmy.getCollisionRectangle().getY(), jimmy.getCollisionRectangle().getWidth(),
                jimmy.getCollisionRectangle().getHeight(), satelite1.getCollisionRectangle().getX(),
                satelite1.getCollisionRectangle().getY(), satelite1.getCollisionRectangle().getWidth(),
                satelite1.getCollisionRectangle().getHeight())) {

        }
    }
   /* for (SateliteTwo satelite2 : sateliteDos) {
        if (checkCollisions(jimmy.getCollisionRectangle().getX(), jimmy.getCollisionRectangle().getY(), jimmy.getCollisionRectangle().getWidth(),
                jimmy.getCollisionRectangle().getHeight(), satelite2.getCollisionRectangle().getX(),
                satelite2.getCollisionRectangle().getY(), satelite2.getCollisionRectangle().getWidth(),
                satelite2.getCollisionRectangle().getHeight())) {

        }
    }*/



    ///////////////////////////////////////////
    ///////////////////////////////////////////
    /////////////////////ITEMS//////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////


    for (BlueDiamond item : itemseses) {
        if (checkCollisions(jimmy.getCollisionRectangle().getX(), jimmy.getCollisionRectangle().getY(), jimmy.getCollisionRectangle().getWidth(),
                jimmy.getCollisionRectangle().getHeight(), item.getCollisionRectangle().getX(),
                item.getCollisionRectangle().getY(), item.getCollisionRectangle().getWidth(),
                item.getCollisionRectangle().getHeight())) {
             }
        }
    for (GreenDiamond item1 : itemsgreen) {
        if (checkCollisions(jimmy.getCollisionRectangle().getX(), jimmy.getCollisionRectangle().getY(), jimmy.getCollisionRectangle().getWidth(),
                jimmy.getCollisionRectangle().getHeight(), item1.getCollisionRectangle().getX(),
                item1.getCollisionRectangle().getY(), item1.getCollisionRectangle().getWidth(),
                item1.getCollisionRectangle().getHeight())) {
        }
        for (RedDiamond item2 : itemsred) {
            if (checkCollisions(jimmy.getCollisionRectangle().getX(), jimmy.getCollisionRectangle().getY(), jimmy.getCollisionRectangle().getWidth(),
                    jimmy.getCollisionRectangle().getHeight(), item2.getCollisionRectangle().getX(),
                    item2.getCollisionRectangle().getY(), item2.getCollisionRectangle().getWidth(),
                    item2.getCollisionRectangle().getHeight())) {
            }
        }
        for (PinkDiamond item3 : itemspink) {
            if (checkCollisions(jimmy.getCollisionRectangle().getX(), jimmy.getCollisionRectangle().getY(), jimmy.getCollisionRectangle().getWidth(),
                    jimmy.getCollisionRectangle().getHeight(), item3.getCollisionRectangle().getX(),
                    item3.getCollisionRectangle().getY(), item3.getCollisionRectangle().getWidth(),
                    item3.getCollisionRectangle().getHeight())) {
            }
        }
        for (YellowDiamond item4 : itemsyel) {
            if (checkCollisions(jimmy.getCollisionRectangle().getX(), jimmy.getCollisionRectangle().getY(), jimmy.getCollisionRectangle().getWidth(),
                    jimmy.getCollisionRectangle().getHeight(), item4.getCollisionRectangle().getX(),
                    item4.getCollisionRectangle().getY(), item4.getCollisionRectangle().getWidth(),
                    item4.getCollisionRectangle().getHeight())) {
            }
        }

        for (BananaOne item5 : itemsBanana) {
            if (checkCollisions(jimmy.getCollisionRectangle().getX(), jimmy.getCollisionRectangle().getY(), jimmy.getCollisionRectangle().getWidth(),
                    jimmy.getCollisionRectangle().getHeight(), item5.getCollisionRectangle().getX(),
                    item5.getCollisionRectangle().getY(), item5.getCollisionRectangle().getWidth(),
                    item5.getCollisionRectangle().getHeight())) {
            }
        }

    }




}


    private void update(float delta) {


        if(state == PLAYING){
            jimmy.update(delta);
            stopJimLeavingTheScreen();
            jimmy.setPosition(jimmy.getX(),BARRAPOS - (TAMCHAN +30) - velocity_y);
            updateCameraY(velocity_y);
            velocity_y = velocity_y+6;

            JimCollisions();
            handleJimCollisionWithDiamonds();
            handleJimCollisionWithAsteroids();
        }



        if (jimmy.getY() <= 800) {
            state = GAME_WON;

            camera.position.set(0,800,0);
            game.getAssetManager().get("SpaceLevelSong.mp3", Music.class).stop();
            if (game.isMusicOn()){
                game.getAssetManager().get("WIN.wav", Sound.class).play();}



        }

        if (vidas <= 0) {
            state = GAME_OVER;             game.getAssetManager().get("SpaceLevelSong.mp3", Music.class).stop();             if (game.isMusicOn()){                 game.getAssetManager().get("ASCENDFX.wav", Sound.class).play();    }


        }

        }

    private void addPointsBlue(){
        score+= sumaBlue;
        scoreNumber.setText(Integer.toString(score));

    }
    private void addPointsRed(){
        score+= sumaRed;
        scoreNumber.setText(Integer.toString(score));

    }
    private void addPointsYellow(){
        score+= sumaYellow;
        scoreNumber.setText(Integer.toString(score));

    }
    private void addPointsPink(){
        score+= sumaPink;
        scoreNumber.setText(Integer.toString(score));

    }
    private void addPointsGreen(){
        score+= sumaGreen;
        scoreNumber.setText(Integer.toString(score));

    }

    private void giveMeMyBanana(){
        vidas= vidas+1;
        score+= restObst;
        life.setText(Integer.toString(vidas));
        scoreNumber.setText(Integer.toString(score));

    }



    private void dieMonkeyDie(){
        vidas-= restvida;
        score-= restObst;
        life.setText(Integer.toString(vidas));
        scoreNumber.setText(Integer.toString(score));

    }


    private void drawScore() {

        font.titulo(batch, Integer.toString(score), 250, BARRA_SIZE-velocity_y);
    }

    private boolean checkCollisions(float Ax, float Ay, float Aw, float Ah, float Bx, float By, float Bw, float Bh){
        if( Ay + Ah < By) return false;
        else if( Ay > By + Bh) return false;
        else if(Ax + Aw < Bx) return false;
        else if(Ax > Bx + Bw) return false;

        return true;
    }

    private void handleJimCollisionWithDiamonds() {


        for (Iterator<BlueDiamond> iter = itemseses.iterator(); iter.hasNext(); ) {
            BlueDiamond item = iter.next();
            if (jimmy.getCollisionRectangle().overlaps(item.getCollisionRectangle())) {
                if (game.isEffectsOn())  {      game.getAssetManager().get("CHIP1FX.wav", Sound.class).play();}
                iter.remove();
                addPointsBlue();
            }
        }

        for (Iterator<RedDiamond> iter1 = itemsred.iterator(); iter1.hasNext(); ) {
            RedDiamond item1 = iter1.next();
            if (jimmy.getCollisionRectangle().overlaps(item1.getCollisionRectangle())) {
                if (game.isEffectsOn())  {      game.getAssetManager().get("CHIP1FX.wav", Sound.class).play();}
                iter1.remove();
                addPointsRed();
            }
        }
        for (Iterator<PinkDiamond> iter2 = itemspink.iterator(); iter2.hasNext(); ) {
            PinkDiamond item2 = iter2.next();
            if (jimmy.getCollisionRectangle().overlaps(item2.getCollisionRectangle())) {
                if (game.isEffectsOn())  {      game.getAssetManager().get("CHIP1FX.wav", Sound.class).play();}
                iter2.remove();
                addPointsPink();
            }
        }
        for (Iterator<GreenDiamond> iter3 = itemsgreen.iterator(); iter3.hasNext(); ) {
            GreenDiamond item3 = iter3.next();
            if (jimmy.getCollisionRectangle().overlaps(item3.getCollisionRectangle())) {
                if (game.isEffectsOn())  {      game.getAssetManager().get("CHIP1FX.wav", Sound.class).play();}
                iter3.remove();
                addPointsGreen();
            }
        }
        for (Iterator<YellowDiamond> iter4 = itemsyel.iterator(); iter4.hasNext(); ) {
            YellowDiamond item4 = iter4.next();
            if (jimmy.getCollisionRectangle().overlaps(item4.getCollisionRectangle())) {
                if (game.isEffectsOn())  {      game.getAssetManager().get("CHIP1FX.wav", Sound.class).play();}
                iter4.remove();
                addPointsYellow();
            }
        }
        for (Iterator<BananaOne> iter5 = itemsBanana.iterator(); iter5.hasNext(); ) {
            BananaOne item5 = iter5.next();
            if (jimmy.getCollisionRectangle().overlaps(item5.getCollisionRectangle())) {
                if(game.isEffectsOn())  {   game.getAssetManager().get("CHIP2FX.wav", Sound.class).play();}
                iter5.remove();
                giveMeMyBanana();
            }
        }




    }

    private void handleJimCollisionWithAsteroids() {
       /* for (Iterator<RockOne> iter = rockUno.iterator(); iter.hasNext(); ) {
            RockOne rock = iter.next();
            if (jimmy.getCollisionRectangle().overlaps(rock.getCollisionRectangle())) {
                if(game.isEffectsOn()) {  game.getAssetManager().get("CRASH1FX.wav", Sound.class).play(); }
                iter.remove();
                dieMonkeyDie();
            }
        }
        */
        for (Iterator<RockTwo> iter1 = rockDos.iterator(); iter1.hasNext(); ) {
            RockTwo rock1 = iter1.next();
            if (jimmy.getCollisionRectangle().overlaps(rock1.getCollisionRectangle())) {
                if(game.isEffectsOn()) {  game.getAssetManager().get("CRASH2FX.wav", Sound.class).play(); }
                iter1.remove();
                dieMonkeyDie();
            }
        }
        for (Iterator<RocketOne> iter2 = rocketUno.iterator(); iter2.hasNext(); ) {
            RocketOne rocket1 = iter2.next();
            if (jimmy.getCollisionRectangle().overlaps(rocket1.getCollisionRectangle())) {
                if(game.isEffectsOn()) {  game.getAssetManager().get("CRASH1FX.wav", Sound.class).play(); }
                iter2.remove();
                dieMonkeyDie();
            }
        }

        for (Iterator<RocketThree> iter3 = rocketTres.iterator(); iter3.hasNext(); ) {
            RocketThree rocket3 = iter3.next();
            if (jimmy.getCollisionRectangle().overlaps(rocket3.getCollisionRectangle())) {
                if(game.isEffectsOn()) {  game.getAssetManager().get("CRASH2FX.wav", Sound.class).play(); }
                iter3.remove();
                dieMonkeyDie();
            }
        }

        for (Iterator<SateliteOne> iter4 = sateliteUno.iterator(); iter4.hasNext(); ) {
            SateliteOne satelite1 = iter4.next();
            if (jimmy.getCollisionRectangle().overlaps(satelite1.getCollisionRectangle())) {
                if(game.isEffectsOn()) {  game.getAssetManager().get("CRASH1FX.wav", Sound.class).play(); }
                iter4.remove();
                dieMonkeyDie();
            }
        }
        /*for (Iterator<SateliteTwo> iter5 = sateliteDos.iterator(); iter5.hasNext(); ) {
            SateliteTwo satelite2 = iter5.next();
            if (jimmy.getCollisionRectangle().overlaps(satelite2.getCollisionRectangle())) {
                if(game.isEffectsOn()) {  game.getAssetManager().get("CRASH2FX.wav", Sound.class).play(); }
                iter5.remove();
                dieMonkeyDie();
            }
        }*/



    }


    private void stopJimLeavingTheScreen() {
        if (jimmy.getY() < 0) {
            jimmy.setPosition(jimmy.getX(), 0);
        }
        if (jimmy.getX() < 0) {
            jimmy.setPosition(0, jimmy.getY());
        }

        TiledMapTileLayer tiledMapTileLayer = (TiledMapTileLayer) map.getLayers().get(0);
        float levelWidth = tiledMapTileLayer.getWidth() * tiledMapTileLayer.getTileWidth();
        if (jimmy.getX() + Jim.WIDTH > levelWidth) {
            jimmy.setPosition(levelWidth - Jim.WIDTH, jimmy.getY());
        }


    }


}

