package mx.itesm.spacedivers.Tournament;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;

import mx.itesm.spacedivers.Menu.StartScreen;
import mx.itesm.spacedivers.SpaceDivers;

/**
 * Created by SpaceDivers on 2/28/2017.
 */

public class ScoreTour extends ScreenAdapter {


    private final SpaceDivers game;
    private static final float WORLD_WIDTH = 720;
    private static final float WORLD_HEIGHT = 1280;

    private Stage stage;
    private Texture backgroundTexture;
    private Texture backTexture;
    private Texture backPressedTexture;
    private Texture scrollableTexture;
    private Texture playTexture;
    private Texture playPressedTexture;
    private Texture pantallaHowTo;
    private Image imagenHowTo;
    private Label highscoreNumberTour;
    private BitmapFont bitmapFont;





    private ImageButton backButton;
    private ImageButton play;


    public ScoreTour(SpaceDivers game){
        this.game = game;
    }

    @Override
    public void show() {
        super.show();
        stage = new Stage(new FitViewport(WORLD_WIDTH, WORLD_HEIGHT));
        Gdx.input.setInputProcessor(stage);

        backgroundTexture = new Texture(Gdx.files.internal("FondoPrinc.jpeg"));
        Image background = new Image(backgroundTexture);
        stage.addActor(background);

        pantallaHowTo = new Texture(Gdx.files.internal("Youdidit.png"));
        imagenHowTo = new Image(new TextureRegionDrawable(new TextureRegion(pantallaHowTo)));

        bitmapFont = new BitmapFont(Gdx.files.internal("Titu.fnt"));

        /*scrollableTexture = new Texture(Gdx.files.internal("WelcomeMercury.png"));

       Image howto = new Image(scrollableTexture);

        ScrollPane sp = new ScrollPane(howto);

        sp.setWidth(howto.getWidth());
        sp.setHeight(690);
        sp.setPosition((WORLD_WIDTH-sp.getWidth())/2, ((WORLD_HEIGHT-sp.getHeight())/2)-33);
*/
        final Preferences preferencias = Gdx.app.getPreferences("Highscores");
        int scorePasadoT = preferencias.getInteger("temp");

        Label.LabelStyle highscore = new Label.LabelStyle(bitmapFont,Color.WHITE);
        highscoreNumberTour = new Label(Integer.toString(scorePasadoT),highscore);
        highscoreNumberTour.setPosition(400, (WORLD_HEIGHT/4),Align.center);

        playTexture = new Texture(Gdx.files.internal("BotonContinueLibre.png"));
        playPressedTexture = new Texture(Gdx.files.internal("BotonContinuePressed.png"));


        backTexture = new Texture(Gdx.files.internal("Back.png"));
        backPressedTexture = new Texture(Gdx.files.internal("BackPress.png"));

        backButton = new ImageButton(new TextureRegionDrawable(new TextureRegion(backTexture)), new TextureRegionDrawable(new TextureRegion(backPressedTexture)));
        backButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                game.setScreen(new WelcomeTour(game));
                dispose();
            }
        });

        backButton.setPosition(10, (WORLD_HEIGHT/6)-40, Align.left);


        play = new ImageButton(new TextureRegionDrawable(new TextureRegion(playTexture)), new TextureRegionDrawable(new TextureRegion(playPressedTexture)));
        play.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                preferencias.putInteger("temp",0);

                game.setScreen(new StartScreen(game));

                dispose();
            }
        });
        play.setPosition(375, (WORLD_HEIGHT/6)-40, Align.center);

        stage.addActor(imagenHowTo);
        imagenHowTo.setPosition(0,200);
        stage.addActor(backButton);

        stage.addActor(imagenHowTo);
        stage.addActor(play);
        stage.addActor(highscoreNumberTour);
        highscoreNumberTour.setPosition(WORLD_WIDTH/2,400,Align.center);





    }



    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        clearScreen();
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void dispose() {
        super.dispose();
        stage.dispose();
        backgroundTexture.dispose();
        backTexture.dispose();
       // scrollableTexture.dispose();
        playTexture.dispose();
        playPressedTexture.dispose();
    }

    private void clearScreen() {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }


}