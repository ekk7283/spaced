package mx.itesm.spacedivers.Tournament.Venus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.Random;

import mx.itesm.spacedivers.Tournament.Venus.VenusTour1;
import mx.itesm.spacedivers.SpaceDivers;

/**
 * Created by spider-i on 3/26/17.
 */

public class LoadingScreenVenusTour extends ScreenAdapter {
    private static final float WORLD_WIDTH = 720;
    private static final float WORLD_HEIGHT = 1280;
    private static final float PROGRESS_BAR_WIDTH = 200;
    private static final float PROGRESS_BAR_HEIGHT = 25;
    private ShapeRenderer shapeRenderer;
    private Viewport viewport;
    private OrthographicCamera
            camera;
    private float progress = 0;

    private final SpaceDivers game;

    private Random rand;

    private int aleatorio;

    public LoadingScreenVenusTour(SpaceDivers game) {
        this.game = game;
    }


    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void show() {
        camera = new OrthographicCamera();
        camera.position.set(WORLD_WIDTH / 2, WORLD_HEIGHT / 2, 0);
        camera.update();


        viewport = new FitViewport(WORLD_WIDTH, WORLD_HEIGHT, camera);
        shapeRenderer = new ShapeRenderer();
        rand = new Random();

        game.getAssetManager().load("mart.png", Texture.class);
        game.getAssetManager().load("merc.png", Texture.class);
        game.getAssetManager().load("VenusMap.tmx", TiledMap.class);


        game.getAssetManager().load("BarraSuperior.png",Texture.class);
        game.getAssetManager().load("SpaceLevelSong.mp3", Music.class);

        game.getAssetManager().load("CHIP2FX.wav",Sound.class);
        game.getAssetManager().load("CHIP1FX.wav",Sound.class);
        game.getAssetManager().load("CRASH1FX.wav",Sound.class);
        game.getAssetManager().load("CRASH2FX.wav",Sound.class);
        game.getAssetManager().load("ASCENDFX.wav",Sound.class);
        game.getAssetManager().load("UNLOCKFX.wav",Sound.class);
        game.getAssetManager().load("WIN.wav",Sound.class);





        game.getAssetManager().load("jim1.png",Texture.class);

        game.getAssetManager().load("roca2anim.png", Texture.class);
        game.getAssetManager().load("roca1anim.png", Texture.class);
        game.getAssetManager().load("Nave.png", Texture.class);
        game.getAssetManager().load("Nave1.png", Texture.class);
        game.getAssetManager().load("Nave2.png", Texture.class);
        game.getAssetManager().load("AguaAnim.png", Texture.class);
        game.getAssetManager().load("asteroide.png", Texture.class);
        game.getAssetManager().load("hielo.png", Texture.class);
        game.getAssetManager().load("gas2tile.png", Texture.class);




        game.getAssetManager().load("GameOver.png",Texture.class);
        game.getAssetManager().load("Paused.png", Texture.class);
        game.getAssetManager().load("Victory.png",Texture.class);
        game.getAssetManager().load("Youdidit.png",Texture.class);



        game.getAssetManager().load("DiamAzul.png",Texture.class);
        game.getAssetManager().load("DiamAmarillo.png",Texture.class);
        game.getAssetManager().load("DiamRojo.png",Texture.class);
        game.getAssetManager().load("DiamRosa.png",Texture.class);
        game.getAssetManager().load("DiamVerde.png",Texture.class);
        game.getAssetManager().load("bananas.png",Texture.class);





    }


    @Override
    public void render(float delta) {
        doRandom();
       update();
        clearScreen();
        draw();
    }


    private int doRandom( ){
         aleatorio = rand.nextInt(2);
        return aleatorio;

    }

    @Override
    public void dispose() {
        shapeRenderer.dispose();
    }

    private void update() {
        if (game.getAssetManager().update()) {
            game.setScreen(new VenusTour1(game,aleatorio));

        } else {
            progress = game.getAssetManager().getProgress();
        }
    }

    private void clearScreen() {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    private void draw() {
        shapeRenderer.setProjectionMatrix(camera.projection);
        shapeRenderer.setTransformMatrix(camera.view);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.rect(
                (WORLD_WIDTH  - PROGRESS_BAR_WIDTH) / 2, WORLD_HEIGHT / 2 - PROGRESS_BAR_HEIGHT / 2,
                progress * PROGRESS_BAR_WIDTH, PROGRESS_BAR_HEIGHT);
        shapeRenderer.end();
    }
}