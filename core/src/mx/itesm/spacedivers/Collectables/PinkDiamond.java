package mx.itesm.spacedivers.Collectables;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by spider-i on 4/3/17.
 */

public class PinkDiamond {

        public static final int WIDTH = 128;
        public static final int HEIGHT = 128;

        private final Rectangle collision;

        private final Texture texture;
        private final float x;
        private final float y;
    private float animationTimer = 0;
    private final Animation blink;
    private final TextureRegion moving;
    private final TextureRegion moving1;
    private final TextureRegion moving2;


        public PinkDiamond(Texture texture, float x, float y) {
            this.texture = texture;
            this.x = x;
            this.y = y;
            this.collision = new Rectangle(x,y, WIDTH-10,HEIGHT-10);
            TextureRegion[] regions = TextureRegion.split(texture, WIDTH, HEIGHT)[0];

            blink = new Animation(0.05F, regions[0], regions[1],regions[2]);
            blink.setPlayMode(Animation.PlayMode.LOOP);
            moving = regions [0];
            moving1= regions [1];
            moving2= regions [2];


        }

    public void update(float delta) {
        animationTimer += delta;
    }

        public Rectangle getCollisionRectangle() {
            return collision;
        }

        public void draw(Batch batch) {
            TextureRegion toDraw = (TextureRegion) blink.getKeyFrame(animationTimer);
            batch.draw(toDraw, x, y);
        }

    }



