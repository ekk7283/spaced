package mx.itesm.spacedivers;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;

import mx.itesm.spacedivers.QuickGames.Earth.LoadingScreenEarthQuick;
import mx.itesm.spacedivers.QuickGames.Mars.LoadingScreenMarsQuick;
import mx.itesm.spacedivers.QuickGames.Mercury.LoadingScreenMercuryQuick;
import mx.itesm.spacedivers.Tournament.Venus.LoadingScreenVenusTour;

public class SpaceDivers extends Game {

	private final AssetManager assetManager = new AssetManager();
	private boolean musicOn=true;
	private boolean effectsOn=true;


	@Override
	public void create(){
		assetManager.setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));
		setScreen(new Transition(this));
	}

	public AssetManager getAssetManager() {
		return assetManager;
	}

	public boolean isMusicOn(){
		return musicOn;
	}

	public void setMusicOn(boolean onOff){
		musicOn=onOff;

	}

	public boolean isEffectsOn(){
		return effectsOn;
	}

	public void setEffectsOn(boolean onOff){
		effectsOn=onOff;

	}




}
