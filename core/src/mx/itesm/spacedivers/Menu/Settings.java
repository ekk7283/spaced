package mx.itesm.spacedivers.Menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import mx.itesm.spacedivers.Fuente;
import mx.itesm.spacedivers.SpaceDivers;

/**
 * Created by SpaceDivers on 2/28/2017.
 */

public class Settings extends ScreenAdapter {

    private final SpaceDivers game;
    private static final float WORLD_WIDTH = 720;
    private static final float WORLD_HEIGHT = 1280;

    private SpriteBatch spriteBatch;
    private Fuente font;

    private Stage stage;
    private Texture backgroundTexture;
    private Texture backTexture;
    private Texture backPressedTexture;
    private Texture sunTexture;
    private Texture sunPressTexture;
    private int musica =1;
    private boolean fects  = true;


    private BitmapFont bitmapFont;


    private Label effectsLab;
    private Label musicLab;
    private Label settLab;

    private Table table;
    private ImageButton backButton;

    public Settings(SpaceDivers game){
        this.game = game;
    }

    @Override
    public void show() {
        super.show();
        stage = new Stage(new FitViewport(WORLD_WIDTH, WORLD_HEIGHT));
        Gdx.input.setInputProcessor(stage);

        backgroundTexture = new Texture(Gdx.files.internal("FondoPrinc.jpeg"));
        Image background = new Image(backgroundTexture);
        stage.addActor(background);


        spriteBatch = new SpriteBatch();

        font = new Fuente();

        bitmapFont = new BitmapFont(Gdx.files.internal("Titu.fnt"));

        Label.LabelStyle music = new Label.LabelStyle(bitmapFont,Color.WHITE);
        musicLab = new Label("MUSIC",music);
        musicLab.setPosition((WORLD_WIDTH/2), 180,Align.center);


        Label.LabelStyle effects = new Label.LabelStyle(bitmapFont,Color.WHITE);
        effectsLab = new Label("EFFECTS",effects);
        effectsLab.setPosition((WORLD_WIDTH/2), 650,Align.center);
        effectsLab.scaleBy(-37.3f);


        Label.LabelStyle sett = new Label.LabelStyle(bitmapFont,Color.WHITE);
        settLab = new Label("SETTINGS",sett);
        settLab.setPosition((WORLD_WIDTH/2), 1150,Align.center);




        backTexture = new Texture(Gdx.files.internal("Back.png"));
        backPressedTexture = new Texture(Gdx.files.internal("BackPress.png"));

        sunTexture = new Texture(Gdx.files.internal("Son.png"));
        sunPressTexture = new Texture(Gdx.files.internal("SonPress.png"));
        final ImageButton fx = new ImageButton(new TextureRegionDrawable(new TextureRegion(sunTexture)), new TextureRegionDrawable(new TextureRegion(sunTexture)), new TextureRegionDrawable(new TextureRegion(sunPressTexture)));
        fx.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
               game.setEffectsOn(!game.isEffectsOn());
               // fx. (new ImageButton (new TextureRegionDrawable(new TextureRegion(sunPressTexture))));

            }
        });

        sunTexture = new Texture(Gdx.files.internal("Son.png"));
        sunPressTexture = new Texture(Gdx.files.internal("SonPress.png"));

        final ImageButton musicOn = new ImageButton(new TextureRegionDrawable(new TextureRegion(sunTexture)), new TextureRegionDrawable(new TextureRegion(sunTexture)), new TextureRegionDrawable(new TextureRegion(sunPressTexture)));
        musicOn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);

                game.setMusicOn(!game.isMusicOn());
                if (game.isMusicOn()==false) {
                    game.getAssetManager().get("MainTheme.mp3", Music.class).stop();
                }
                if (game.isMusicOn()==true) {
                    game.getAssetManager().get("MainTheme.mp3", Music.class).play();
                }


            }});



        backButton = new ImageButton(new TextureRegionDrawable(new TextureRegion(backTexture)), new TextureRegionDrawable(new TextureRegion(backPressedTexture)));
        backButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                game.setScreen(new mx.itesm.spacedivers.Menu.StartScreen(game));
                dispose();
            }
        });



        backButton.setPosition(10, (WORLD_HEIGHT/6)-40, Align.left);


        table = new Table();
        table.row();
        table.add(fx).padTop(10f).center().size(400f);
        table.row();
        table.add(musicOn).padTop(50f).center().size(400f);
        table.padBottom(40f);


        table.setFillParent(true);
        table.pack();
        stage.addActor(table);

        stage.addActor(effectsLab);
        stage.addActor(musicLab);
        stage.addActor(settLab);
        stage.addActor(backButton);


    }


    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        clearScreen();
        stage.act(delta);
        stage.draw();

        spriteBatch.begin();
        spriteBatch.end();
    }

    @Override
    public void dispose() {
        super.dispose();
        stage.dispose();
        backgroundTexture.dispose();
        backTexture.dispose();
        sunTexture.dispose();
        sunPressTexture.dispose();
        backPressedTexture.dispose();
    }

    private void clearScreen() {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

}
