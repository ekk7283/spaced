package mx.itesm.spacedivers.Menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;

import mx.itesm.spacedivers.SpaceDivers;

/**
 * Created by SpaceDivers on 2/28/2017.
 */

public class Credits extends ScreenAdapter {

    private final SpaceDivers game;
    private static final float WORLD_WIDTH = 720;
    private static final float WORLD_HEIGHT = 1280;

    private Stage stage;
    private Texture backgroundCreditosTexture;
    private Texture backTexture;
    private Texture backPressedTexture;


    private ImageButton backButton;

    public Credits(SpaceDivers game){
        this.game = game;
    }

    @Override
    public void show() {
        super.show();
        stage = new Stage(new FitViewport(WORLD_WIDTH, WORLD_HEIGHT));
        Gdx.input.setInputProcessor(stage);

        backgroundCreditosTexture = new Texture(Gdx.files.internal("creditosf.jpg"));
        Image background = new Image(backgroundCreditosTexture);
        stage.addActor(background);


        backTexture = new Texture(Gdx.files.internal("Back.png"));
        backPressedTexture = new Texture(Gdx.files.internal("BackPress.png"));

        backButton = new ImageButton(new TextureRegionDrawable(new TextureRegion(backTexture)), new TextureRegionDrawable(new TextureRegion(backPressedTexture)));
        backButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                game.setScreen(new StartScreen(game));
                dispose();
            }
        });

        backButton.setPosition(10, (WORLD_HEIGHT/6)-40, Align.left);
        stage.addActor(backButton);

    }


    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        clearScreen();
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void dispose() {
        super.dispose();
        stage.dispose();
        backgroundCreditosTexture.dispose();
        backTexture.dispose();

    }

    private void clearScreen() {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }


}
