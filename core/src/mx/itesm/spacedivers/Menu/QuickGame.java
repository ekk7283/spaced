package mx.itesm.spacedivers.Menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import mx.itesm.spacedivers.Fuente;
import mx.itesm.spacedivers.QuickGames.Earth.WelcomeEarth;
import mx.itesm.spacedivers.QuickGames.Mars.WelcomeMars;
import mx.itesm.spacedivers.QuickGames.Mercury.WelcomeMercury;
import mx.itesm.spacedivers.QuickGames.Venus.WelcomeVenus;
import mx.itesm.spacedivers.SpaceDivers;


/**
 * Created by SpaceDivers on 2/28/2017.
 */

public class QuickGame extends ScreenAdapter {

    private final SpaceDivers game;
    private static final float WORLD_WIDTH = 720;
    private static final float WORLD_HEIGHT = 1280;

    private Stage stage;

    private SpriteBatch spriteBatch;
    private BitmapFont bitmapFont;


    private Texture backgroundTexture;
    private Texture backTexture;
    private Texture backPressedTexture;
    private Texture planetOne;
    private Texture forwardTexture;
    private Texture forwardPressTexture;
    private Texture planetTwo;
    private Texture planetThree;
    private Texture planetFour;
    private Texture playTexture;
    private Texture playPressTexture;

    private Fuente font;

    private int suma=0;

    private Image planet1;
    private Image planet2;
    private Image planet3;
    private Image planet4;

    private Label highscoreNumberT;
    private Label highscoreNumberM;
    private Label highscoreNumberMe;
    private Label highscoreNumberV;

    private Array<ImageButton> plays;


    private ImageButton backButton;
    private ImageButton leftButton;
    private ImageButton rightButton;

    private ImageButton Play1;
    private ImageButton Play2;
    private ImageButton Play3;
    private ImageButton Play4;

    public QuickGame(SpaceDivers game){
        this.game = game;
    }

    @Override
    public void show() {
        super.show();
        stage = new Stage(new FitViewport(WORLD_WIDTH, WORLD_HEIGHT));
        Gdx.input.setInputProcessor(stage);

        spriteBatch = new SpriteBatch();

        backgroundTexture = new Texture(Gdx.files.internal("FondoPrinc.jpeg"));
        Image background = new Image(backgroundTexture);
        stage.addActor(background);

        backTexture = new Texture(Gdx.files.internal("Back.png"));
        backPressedTexture = new Texture(Gdx.files.internal("BackPress.png"));

        forwardTexture = new Texture(Gdx.files.internal("Forward.png"));
        forwardPressTexture = new Texture(Gdx.files.internal("ForwardPress.png"));

        playTexture = new Texture(Gdx.files.internal("Pla1.png"));
        playPressTexture = new Texture(Gdx.files.internal("Pla2.png"));


        planetOne = new Texture(Gdx.files.internal("q1.png"));
       planet1 = new Image(planetOne);

        planetTwo = new Texture(Gdx.files.internal("q4.png"));
        planet2 = new Image(planetTwo);
        planetThree = new Texture(Gdx.files.internal("q3.png"));
        planet3 = new Image(planetThree);

        planetFour = new Texture(Gdx.files.internal("q2.png"));
        planet4= new Image(planetFour);

        plays = new Array<ImageButton>();
        plays.add(Play1);
        plays.add(Play2);
        plays.add(Play3);
        plays.add(Play4);

        font = new Fuente();



        bitmapFont = new BitmapFont(Gdx.files.internal("Titu.fnt"));


        //////////PREF INICIAL//////////////

        Preferences preferencias = Gdx.app.getPreferences("Highscores");
        int scorePasadoT = preferencias.getInteger("tierra");
        int scorePasadoM = preferencias.getInteger("marte");
        int scorePasadoMe = preferencias.getInteger("mercurio");
        int scorePasadoV = preferencias.getInteger("venus");


        Label.LabelStyle tierra = new Label.LabelStyle(bitmapFont,Color.WHITE);
        highscoreNumberT = new Label(Integer.toString(scorePasadoT),tierra);
        highscoreNumberT.setPosition(365, 420,Align.center);

        Label.LabelStyle marte = new Label.LabelStyle(bitmapFont,Color.WHITE);
        highscoreNumberM = new Label(Integer.toString(scorePasadoM),marte);
        highscoreNumberM.setPosition(365, 420,Align.center);

        Label.LabelStyle mercurio = new Label.LabelStyle(bitmapFont,Color.WHITE);
        highscoreNumberMe = new Label(Integer.toString(scorePasadoMe),mercurio);
        highscoreNumberMe.setPosition(365, 420,Align.center);

        Label.LabelStyle venus = new Label.LabelStyle(bitmapFont,Color.WHITE);
        highscoreNumberV = new Label(Integer.toString(scorePasadoV),venus);
        highscoreNumberV.setPosition(365, 420,Align.center);

        stage.addActor(planet1);



        ////////////////////

        backButton = new ImageButton(new TextureRegionDrawable(new TextureRegion(backTexture)), new TextureRegionDrawable(new TextureRegion(backPressedTexture)));
        backButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                game.setScreen(new mx.itesm.spacedivers.Menu.StartScreen(game));
                dispose();

            }
        });




        planet1.setPosition(0,250);
        planet2.setPosition(0,250);
        planet3.setPosition(0,250);
        planet4.setPosition(0,250);


        Play1 = new ImageButton(new TextureRegionDrawable(new TextureRegion(playTexture)), new TextureRegionDrawable(new TextureRegion(playPressTexture)));
        Play1.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                game.setScreen(new WelcomeEarth(game));
                dispose();

            }
        });
        Play2 = new ImageButton(new TextureRegionDrawable(new TextureRegion(playTexture)), new TextureRegionDrawable(new TextureRegion(playPressTexture)));
        Play2.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                game.setScreen(new WelcomeMars(game));
                dispose();

            }
        });
        Play3 = new ImageButton(new TextureRegionDrawable(new TextureRegion(playTexture)), new TextureRegionDrawable(new TextureRegion(playPressTexture)));
        Play3.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                game.setScreen(new WelcomeMercury(game));
                dispose();

            }
        });
        Play4 = new ImageButton(new TextureRegionDrawable(new TextureRegion(playTexture)), new TextureRegionDrawable(new TextureRegion(playPressTexture)));
        Play4.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                game.setScreen(new WelcomeVenus(game));
                dispose();

            }
        });


        Play1.setPosition(400, (WORLD_HEIGHT/6)-40,Align.center);
        Play2.setPosition(400, (WORLD_HEIGHT/6)-40,Align.center);
        Play3.setPosition(400, (WORLD_HEIGHT/6)-40,Align.center);
        Play4.setPosition(400, (WORLD_HEIGHT/6)-40,Align.center);

        stage.addActor(backButton);

        stage.addActor(Play1);

        stage.addActor(highscoreNumberT);
        backButton.setPosition(10, (WORLD_HEIGHT/6)-40, Align.left);




    }





    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        stage.getViewport().update(width, height, true);
    }

    public void moveNiveles(float delta){

        leftButton = new ImageButton(new TextureRegionDrawable(new TextureRegion(backTexture)), new TextureRegionDrawable(new TextureRegion(backPressedTexture)));
        leftButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);

                switch (suma){

                    case 0:
                        removeTierra();
                        addVenus();
                        suma=3;
                        break;

                    case 1:
                        removeMarte();
                        addTierra();
                        suma=0;

                        break;

                    case 2:
                        removeMercurio();
                        addMarte();
                        suma=1;

                        break;

                    case 3:
                        removeVenus();
                        addMercurio();
                        suma=2;

                        break;


                }
            }
        });
        rightButton = new ImageButton(new TextureRegionDrawable(new TextureRegion(forwardTexture)), new TextureRegionDrawable(new TextureRegion(forwardPressTexture)));
        rightButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                switch (suma) {

                    case 0:
                        removeTierra();
                        addMarte();
                        suma = 1;
                        break;

                    case 1:
                        removeMarte();
                        addMercurio();
                        suma = 2;

                        break;

                    case 2:
                        removeMercurio();
                        addVenus();
                        suma = 3;

                        break;

                    case 3:
                        removeVenus();
                        addTierra();
                        suma = 0;

                        break;


                }
            }
        });


        stage.addActor(leftButton);
        leftButton.setPosition(10, (WORLD_HEIGHT/2), Align.left);
        stage.addActor(rightButton);
        rightButton.setPosition(700, (WORLD_HEIGHT/2), Align.right);





    }


    @Override
    public void render(float delta) {
        super.render(delta);
        clearScreen();
        stage.act(delta);
        moveNiveles(delta);
        stage.draw();
        spriteBatch.begin();
        //font.titulo(spriteBatch, "QUICKGAME", (WORLD_WIDTH/2)+200, 1750);
        //font.titulo(spriteBatch, "HIGHSCORE", (WORLD_WIDTH/2)+200, 725);
        spriteBatch.end();

    }

    private void addTierra(){

        stage.addActor(Play1);
        stage.addActor(highscoreNumberT);
        stage.addActor(planet1);

    }
    private void removeTierra(){
        Play1.remove();
        highscoreNumberT.remove();
        planet1.remove();


    }

    private void addMarte(){

        stage.addActor(Play2);
        stage.addActor(highscoreNumberM);
        stage.addActor(planet2);

    }
    private void removeMarte(){
        Play2.remove();
        highscoreNumberM.remove();
        planet2.remove();

    }
    private void addMercurio(){
        stage.addActor(Play3);
        stage.addActor(highscoreNumberMe);
        stage.addActor(planet3);

    }
    private void removeMercurio(){
        Play3.remove();
        highscoreNumberMe.remove();
        planet3.remove();

    }
    private void addVenus(){
        stage.addActor(Play4);
        stage.addActor(highscoreNumberV);
        stage.addActor(planet4);

    }
    private void removeVenus(){
        Play4.remove();
        highscoreNumberV.remove();
        planet4.remove();

    }





    @Override
    public void dispose() {
        super.dispose();
        stage.dispose();
        backgroundTexture.dispose();
        backTexture.dispose();
        forwardPressTexture.dispose();
        planetOne.dispose();
        forwardTexture.dispose();
        planetTwo.dispose();
        planetThree.dispose();
        planetFour.dispose();
        spriteBatch.dispose();




    }

    private void clearScreen() {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }


}
