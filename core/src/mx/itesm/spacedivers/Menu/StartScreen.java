package mx.itesm.spacedivers.Menu;
//hola

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import mx.itesm.spacedivers.Menu.Credits;
import mx.itesm.spacedivers.SpaceDivers;
import mx.itesm.spacedivers.Tournament.WelcomeTour;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;


/**
 * Created by SpaceDivers on 2/28/2017.
 */

public class StartScreen extends ScreenAdapter {

    private final SpaceDivers game;
    private static final float WORLD_WIDTH = 720;
    private static final float WORLD_HEIGHT = 1280;

    private Stage stage;

    private Texture backgroundSpace;
    private Texture logoTexture;
    private Texture playTourTex;
    private Texture playTourPressTex;
    private Texture playQuickTex;
    private Texture playQuickPressTex;
    private Table table;
    private Texture settingsTexture;
    private Texture settingsPressTexture;
    private Texture creditsTexture;
    private Texture creditsPressTexture;




    public StartScreen(SpaceDivers game){
        this.game = game;

        /*preferenciasNiveles.putString("2","roca2 area2 beso2 lol2");
        ;*/


    }

    @Override
    public void show() {
        super.show();
        stage = new Stage(new FitViewport(WORLD_WIDTH, WORLD_HEIGHT));
        Gdx.input.setInputProcessor(stage);


        backgroundSpace = new Texture(Gdx.files.internal("FondoPrinc.jpeg"));
        Image background = new Image(backgroundSpace);
        stage.addActor(background);

        logoTexture = new Texture(Gdx.files.internal("logo-01.png"));
        Image logo = new Image(logoTexture);

        playTourTex = new Texture(Gdx.files.internal("tou1.png"));
        playTourPressTex = new Texture(Gdx.files.internal("tou2.png"));
        ImageButton play = new ImageButton(new TextureRegionDrawable(new TextureRegion(playTourTex)), new TextureRegionDrawable(new TextureRegion(playTourPressTex)));
        play.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                game.setScreen(new WelcomeTour(game));
                dispose();
            }
        });

        settingsTexture = new Texture(Gdx.files.internal("sett1.png"));
        settingsPressTexture = new Texture(Gdx.files.internal("sett2.png"));
        ImageButton options = new ImageButton(new TextureRegionDrawable(new TextureRegion(settingsTexture)), new TextureRegionDrawable(new TextureRegion(settingsPressTexture)));
        options.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                game.setScreen(new Settings(game));
                dispose();
            }
        });

        creditsTexture = new Texture(Gdx.files.internal("cred1.png"));
        creditsPressTexture = new Texture(Gdx.files.internal("cred2.png"));
        ImageButton credits = new ImageButton(new TextureRegionDrawable(new TextureRegion(creditsTexture)), new TextureRegionDrawable(new TextureRegion(creditsPressTexture)));
        credits.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                game.setScreen(new Credits(game));
                dispose();
            }
        });

        playQuickTex = new Texture(Gdx.files.internal("qg1.png"));
        playQuickPressTex = new Texture(Gdx.files.internal("qg2.png"));
        ImageButton quick = new ImageButton(new TextureRegionDrawable(new TextureRegion(playQuickTex)), new TextureRegionDrawable(new TextureRegion(playQuickPressTex)));
        quick.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                game.setScreen(new QuickGame(game));
                dispose();
            }
        });

        // Create table
        table = new Table();
        //table.debug(); //Enables debug

        table.row();
        table.add(logo).padTop(10f).colspan(2);
        table.row();
        table.add(play).padTop(50f).size(320f);
        table.add(quick).padTop(50f).padLeft(10f).size(320f);
        table.row();
        table.add(options).padTop(50f).size(320f);
        table.add(credits).padTop(50f).padLeft(10f).size(320f);
        table.padBottom(40f);

        // Pack table
        table.setFillParent(true);
        table.pack();

        // Set titles's alpha to 0
        logo.getColor().a = 0f;
        options.getColor().a = 0f;
        quick.getColor().a = 0f;
        credits.getColor().a = 0f;
        play.getColor().a = 0f;
        // Adds created table to stage
        stage.addActor(table);
        options.addAction(fadeIn(3f));
        quick.addAction(fadeIn(3f));
        credits.addAction(fadeIn(3f));
        play.addAction(fadeIn(3f));
        logo.addAction(fadeIn(1.6f));

        game.getAssetManager().get("MainTheme.mp3", Music.class);
        if(game.isMusicOn()){ game.getAssetManager().get("MainTheme.mp3", Music.class).play(); }

        Preferences preferencias = Gdx.app.getPreferences("Highscores");

        if (preferencias.get().toString().equals("{}")) {
            preferencias.putInteger("tierra", 0);
            preferencias.putInteger("marte", 0);
            preferencias.putInteger("venus", 0);
            preferencias.putInteger("mercurio", 0);


            preferencias.putInteger("tierrat", 0);
            preferencias.putInteger("martet", 0);
            preferencias.putInteger("mercuriot", 0);

            preferencias.putInteger("tournament", 0);
            preferencias.putInteger("temp", 0);
            preferencias.flush();
        }
        Preferences preferenciasNivelesObst = Gdx.app.getPreferences("niveles");
        if (preferenciasNivelesObst.get().toString().equals("{}")) {
            preferenciasNivelesObst.putString("1", "RocaA1-RocaB1-NaveA1-NaveB1-SateliteA1-SateliteB1");
            preferenciasNivelesObst.putString("2", "RocaA2-RocaB2-NaveA2-NaveB2-SateliteA2-SateliteB2");
            preferenciasNivelesObst.putString("3", "RocaA3-RocaB3-NaveA3-NaveB3-SateliteA3-SateliteB3");
            preferenciasNivelesObst.flush();

        }
    }


    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        clearScreen();
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void dispose() {
        super.dispose();
        stage.dispose();
        backgroundSpace.dispose();
        playTourTex.dispose();
        playTourPressTex.dispose();
        settingsPressTexture.dispose();
        settingsTexture.dispose();
        logoTexture.dispose();
        playQuickPressTex.dispose();
        playQuickTex.dispose();
        creditsPressTexture.dispose();
        creditsTexture.dispose();

    }

    private void clearScreen() {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

}
