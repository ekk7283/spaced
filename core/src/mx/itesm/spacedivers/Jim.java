package mx.itesm.spacedivers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by spider-i on 3/30/17.
 */

public class Jim {

    public static final int WIDTH = 177;
    public static final int HEIGHT = 262;

    private static final float MAX_X_SPEED = 7f;



    private float x = 288;
    private float y = 0;
    private float xSpeed = 0;
    private float ySpeed = 0;

    private final Rectangle collisionRectangle = new Rectangle(x, y, WIDTH-45, HEIGHT-80);


    private float animationTimer = 0;
    private final Animation walking;
    private final TextureRegion standing;
    private final TextureRegion moving;
    private final TextureRegion moving1;




    public Jim(Texture texture) {
        TextureRegion[] regions = TextureRegion.split(texture, WIDTH, HEIGHT)[0];

        walking = new Animation(0.25F, regions[0], regions[1]);
        walking.setPlayMode(Animation.PlayMode.LOOP);
        standing = regions[2];
        moving = regions[0];
        moving1= regions [1];

    }



    public void update(float delta) {
        animationTimer += delta;


        Input input = Gdx.input;

        float accelX = Gdx.input.getAccelerometerX();

        if (input.isKeyPressed(Input.Keys.RIGHT) ||  (accelX <=-2)) {
             xSpeed = MAX_X_SPEED;
        }


        else if (input.isKeyPressed(Input.Keys.LEFT) || (accelX >= 2 )) {
            xSpeed = -MAX_X_SPEED;
        }

        else {
             xSpeed = 0;
        }

        x += xSpeed;
        y += ySpeed;
       updateCollisionRectangle();
    }

    public void drawDebug(ShapeRenderer shapeRenderer) {
        shapeRenderer.rect(collisionRectangle.x, collisionRectangle.y, collisionRectangle.width, collisionRectangle.height);
    }

    public void draw(Batch batch) {
        TextureRegion toDraw = standing;
        if (xSpeed != 0) {
            toDraw = (TextureRegion) walking.getKeyFrame(animationTimer);
        }

        if (xSpeed < 0) {
            if (toDraw.isFlipX()) toDraw.flip(true,false);
        } else if (xSpeed > 0) {
            if (!toDraw.isFlipX()) toDraw.flip(true,false);
        }

        batch.draw(toDraw, x, y);
    }


    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
        updateCollisionRectangle();
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public Rectangle getCollisionRectangle() {
        return collisionRectangle;
    }

    private void updateCollisionRectangle() {
        collisionRectangle.setPosition(x, y);
    }

}