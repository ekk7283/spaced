package mx.itesm.spacedivers;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import mx.itesm.spacedivers.Menu.StartScreen;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by spider-i on 3/13/17.
 */


public class Transition extends ScreenAdapter{

    private final SpaceDivers game;
    private static final float WORLD_WIDTH = 720;
    private static final float WORLD_HEIGHT = 1280;

    private Stage stage;
    private Texture backgroundTexture;
    private Texture logoTexture;

    private float progress=0;



    public Transition(SpaceDivers game){
        this.game = game;
    }

    @Override
    public void show() {
        super.show();
        stage = new Stage(new FitViewport(WORLD_WIDTH, WORLD_HEIGHT));
        Gdx.input.setInputProcessor(stage);

        backgroundTexture = new Texture(Gdx.files.internal("texemblanco.png"));
        logoTexture = new Texture(Gdx.files.internal("logo-01.png"));

        Image background = new Image(backgroundTexture);
        Image logo = new Image(logoTexture);
        stage.addActor(background);
        stage.addActor(logo);
        logo.getColor().a = 0f;

        background.addAction(sequence(delay(2), fadeOut(2)));
        background.setPosition(WORLD_WIDTH/2,WORLD_HEIGHT/2,Align.center);

        logo.setPosition(WORLD_WIDTH/2,WORLD_HEIGHT/2,Align.center);

        logo.addAction(sequence(delay(4), fadeIn(1),delay(1), fadeOut(1), run(new Runnable() {
            @Override
            public void run() {
                game.setScreen(new StartScreen(game));
                dispose();
            }
        })));
        game.getAssetManager().load("MainTheme.mp3",Music.class);

    }
    private void update() {
        if (game.getAssetManager().update()) {

        } else {
            progress = game.getAssetManager().getProgress();
        }
    }


    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        clearScreen();
        update();
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void dispose() {
        super.dispose();
        stage.dispose();
        backgroundTexture.dispose();
        logoTexture.dispose();
    }

    private void clearScreen() {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }


}