package mx.itesm.spacedivers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by spider-i on 2/16/17.
 */

public class Fuente {
    private BitmapFont font;
    private BitmapFont titu;

    public Fuente(){

        font= new BitmapFont(Gdx.files.internal("DarkF.fnt"));
        titu= new BitmapFont(Gdx.files.internal("Titu.fnt"));




    }

    public void mostrarMensaje(SpriteBatch batch, String mensaje, float x, float y){

        GlyphLayout glyph = new GlyphLayout();
        glyph.setText(font, mensaje);
        float anchoTexto = glyph.width;
        font.draw(batch, glyph, x-anchoTexto/2, y);


    }

    public void titulo(SpriteBatch batch, String mensaje, float x, float y){

        GlyphLayout glyph = new GlyphLayout();
        glyph.setText(titu, mensaje);
        float anchoTexto = glyph.width;
        titu.draw(batch, glyph, x-anchoTexto/2, y);


    }
}
