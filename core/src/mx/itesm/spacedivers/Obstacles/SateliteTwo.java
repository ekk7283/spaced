package mx.itesm.spacedivers.Obstacles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by spider-i on 3/31/17.
 */
public class SateliteTwo {

    public static final int WIDTH = 128;
    public static final int HEIGHT = 128;

    private final Rectangle collision;

    private final Texture texture;
    private final float x;
    private final float y;
    private float animationTimer = 0;
    private final Animation rotate;
    private final TextureRegion moving;
    private final TextureRegion moving1;
    private final TextureRegion moving2;
    private final TextureRegion moving3;





    public SateliteTwo(Texture texture, float x, float y) {
        this.texture = texture;
        TextureRegion[] regions = TextureRegion.split(texture, WIDTH, HEIGHT)[0];

        rotate = new Animation(0.15F, regions[0], regions[1],regions[2], regions[3]);
        rotate.setPlayMode(Animation.PlayMode.LOOP);
        moving = regions [0];
        moving1= regions [1];
        moving2= regions [2];
        moving3= regions [3];


        this.x = x;
        this.y = y;
        this.collision = new Rectangle(x,y, WIDTH-30,HEIGHT-30);
    }
    public void update(float delta) {
        animationTimer += delta;
    }

    public Rectangle getCollisionRectangle() {
        return collision;
    }

    public void draw(Batch batch) {

       TextureRegion toDraw = (TextureRegion) rotate.getKeyFrame(animationTimer);
        batch.draw(toDraw, x, y);
    }

}
