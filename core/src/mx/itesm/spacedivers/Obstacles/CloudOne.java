package mx.itesm.spacedivers.Obstacles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by spider-i on 3/31/17.
 */
public class CloudOne {

    public static final int WIDTH = 544;
    public static final int HEIGHT = 352;

    private final Rectangle collision;

    private final Texture texture;
    private final float x;
    private final float y;
    private float animationTimer = 0;
    private final Animation rotate;
    private final TextureRegion moving;




    public CloudOne(Texture texture, float x, float y) {
        this.texture = texture;
        TextureRegion[] regions = TextureRegion.split(texture, WIDTH, HEIGHT)[0];

        rotate = new Animation(0.05F, regions[0]);
        rotate.setPlayMode(Animation.PlayMode.LOOP);
        moving = regions [0];


        this.x = x;
        this.y = y;
        this.collision = new Rectangle(x,y, WIDTH-20,HEIGHT-20);
    }
    public void update(float delta) {
        animationTimer += delta;
    }

    public Rectangle getCollisionRectangle() {
        return collision;
    }

    public void draw(Batch batch) {

       TextureRegion toDraw = (TextureRegion) rotate.getKeyFrame(animationTimer);
        batch.draw(toDraw, x, y);
    }

}
