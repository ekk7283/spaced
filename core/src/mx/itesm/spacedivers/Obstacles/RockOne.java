package mx.itesm.spacedivers.Obstacles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by spider-i on 3/31/17.
 */
public class RockOne {

    public static final int WIDTH = 128;
    public static final int HEIGHT = 128;

    private final Rectangle collision;

    private final Texture texture;
    private final float x;
    private final float y;
    private float animationTimer = 0;
    private final Animation rotate;
    private final TextureRegion moving;
    private final TextureRegion moving1;
    private final TextureRegion moving2;
    private final TextureRegion moving3;
    private final TextureRegion moving4;
    private final TextureRegion moving5;
    private final TextureRegion moving6;
    private final TextureRegion moving7;
    private final TextureRegion moving8;
    private final TextureRegion moving9;
    private final TextureRegion moving10;
    private final TextureRegion moving11;




    public RockOne(Texture texture, float x, float y) {
        this.texture = texture;
        TextureRegion[] regions = TextureRegion.split(texture, WIDTH, HEIGHT)[0];

        rotate = new Animation(0.05F, regions[0], regions[1],regions[2], regions[3],regions[4],
                regions[5],regions[6], regions[7],regions[8],regions[9],
                regions[10],regions[11]);
        rotate.setPlayMode(Animation.PlayMode.LOOP);
        moving = regions [0];
        moving1= regions [1];
        moving2= regions [2];
        moving3= regions [3];
        moving4= regions [4];
        moving5= regions [5];
        moving6= regions [6];
        moving7= regions [7];
        moving8= regions [8];
        moving9= regions [9];
        moving10= regions [10];
        moving11= regions [11];


        this.x = x;
        this.y = y;
        this.collision = new Rectangle(x,y, WIDTH-30,HEIGHT-30);
    }
    public void update(float delta) {
        animationTimer += delta;
    }

    public Rectangle getCollisionRectangle() {
        return collision;
    }

    public void draw(Batch batch) {

       TextureRegion toDraw = (TextureRegion) rotate.getKeyFrame(animationTimer);
        batch.draw(toDraw, x, y);
    }

}
